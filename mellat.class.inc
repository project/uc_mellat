<?php

/**
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 * @file MellatBank class handles all transactions related to Mellat Bank payment service.
 *
 * @see http://www.hejazee.ir/
 * @see http://www.tolooco.com/
 *
 * Contributors: "Mehdi jalali" <jalalimehdi@gmail.com>
 */

/**
 * Mellat Bank Payment System
 */
class MellatBank {
  public $action = 'https://bpm.shaparak.ir/pgwchannel/startpay.mellat';
  public $webMethodURL = 'https://bpm.shaparak.ir/pgwchannel/services/pgw?wsdl';
  public $redirectURL;
  public $totalAmont;
  public $refNum;
  public $resNum;
  public $idMellat;
  protected $payment;
  protected $merchantID;
  protected $userName;
  protected $password;
  protected $msg = array();
  protected $errorState ;
  protected $errorVerify;
  public $style;

  /**
   * Constructor
   */
  function __construct($mID = '',$pass = '',$user = '') {
    require_once dirname(__FILE__) . '/lib/nusoap/nusoap.php';
    
    $this->merchantID = $mID;
    $this->userName = $user;
    $this->password = $pass;
    $this->redirectURL = url('cart/mellat/complete', array('absolute' => TRUE));
    $this->style = array(
      'TableBorderColor' => variable_get('mellat_bank_TableBorderColor', ''),
      'TableBGColor'     => variable_get('mellat_bank_TableBGColor', ''),
      'PageBGColor'      => variable_get('mellat_bank_PageBGColor', ''),
      'PageBorderColor'  => variable_get('mellat_bank_PageBorderColor', ''),
      'TitleFont'        => variable_get('mellat_bank_TitleFont', ''),
      'TitleColor'       => variable_get('mellat_bank_TitleColor', ''),
      'TitleSize'        => variable_get('mellat_bank_TitleSize', ''),
      'TextFont'         => variable_get('mellat_bank_TextFont', ''),
      'TextColor'        => variable_get('mellat_bank_TextColor', ''),
      'TextSize'         => variable_get('mellat_bank_TextSize', ''),
      'TypeTextColor'    => variable_get('mellat_bank_TypeTextColor', ''),
      'TypeTextColor'    => variable_get('mellat_bank_TypeTextColor', ''),
      'TypeTextSize'     => variable_get('mellat_bank_TypeTextSize', ''),
      'LogoURI'          => variable_get('mellat_bank_LogoURI', ''),
      'language'         => variable_get('uc_mellat_website_language', ''),
    );

    $this->errorVerify = array(
      '-1'  => t('The payment has completed but not setteled.'),
      '-2'  => t('Can not call bpSettleRequest internally.'),
      '-3'  => t('Can not call bpVerifyRequest internally.'),
      '-4'  => t('Service can not initiated internally.'),
      '-5'  => t('Remote ID associated with transaction and RefId are not equal.'),
      '-6'  => t('document has already been sent back'),
      '-7'  => t('digital receipt is empty'),
      '-8'  => t('input length is bigger than allowed'),
      '-9'  => t('invalid characters in returned payment'),
      '-10' => t('digital receipt contains invalid characters'),
      '-11' => t('input length is smaller than allowed'),
      '-12' => t('returned payment is negative'),
      '-13' => t('returned payment of partial payment is more than the amount of not returned digital receipt'),
      '-14' => t('such transaction is not defined'),
      '-15' => t('returned payment is entered in decimal'),
      '-16' => t('internal system error'),
      '-17' => t('to refund a transaction done by non-mellat bank cards'),
      '-18' => t('seller IP address is invalid' ),
      '-19' => t('can not call service soap is invalid' ),
      '0' => t('The payment has completed and setteled.'),
      '11'=> t('Incorrect account number.'),
      '12'=> t('Inadequate ballance.'),
      '13'=> t('Incorrect password.'),
      '14'=> t('Too many incorrect password retries.'),
      '15'=> t('Incorrect card.'),
      '16'=> t('Withdrawal times over limit.'),
      '17'=> t('Transaction terminated by user.'),
      '18'=> t('Card expiration date is past.'),
      '19'=> t('Withdrawal amount is excessive'),
      '111' => t('Inalive account provider.'),
      '112' => t('Switch error in account provider.'),
      '113' => t('No response from account provider.'),
      '114' => t('Card owner is not allowed to perform this transaction.'),
      '21'=> t('Invalid transaction from store.'),
      '23'=> t('Security error has occurred.'),
      '24'=> t('Invalide store credentials.'),
      '25'=> t('Invalid amount.'),
      '31'=> t('Invalid response.'),
      '32'=> t('Invalide input format.'),
      '33'=> t('Invalid account.'),
      '34'=> t('System error.'),
      '35'=> t('Invalid date.'),
      '41'=> t('Duplicate transaction identification.'),
      '42'=> t('Sale transaction not found.'),
      '43'=> t('Transaction verified previously.'),
      '44'=> t('Verify transaction not found.'),
      '45'=> t('Transaction setteled previously.'),
      '46'=> t('Transaction is not setteled.'),
      '47'=> t('Settel transaction not found.'),
      '48'=> t('Transaction reversed previously.'),
      '49'=> t('Refund transaction not found.'),
      '412' => t('Invalid receipt ID.'),
      '413'=> t('Invalid payment ID.'),
      '414' => t('Invalid receipt diffuser.'),
      '415' => t('Session has been expired.'),
      '416' => t('Error in storing data.'),
      '417' => t('Invalid payer ID.'),
      '418' => t('Problem in defining user information.'),
      '419' => t('Too many form submission retries.'),
      '421' => t('Invalid IP.'),
      '51'=> t('Duplicate transaction.'),
      '54'=> t('Transaction not found.'),
      '55'=> t('Invalid transaction.'),
      '61'=> t('Error in payment.')
    );

    $this->errorState = array(
      'Canceled By User'                     => t('transaction cancelled by user'),
      'Invalid Amount'                       => t('the amount of returned payment is more than main transaction'),
      'Invalid Transaction'                  => t(' while the main transaction is not found,transaction return request is received'),
      'Invalid Card Number'                  => t('card number is incorrect'),
      'No Such Issuer'                       => t('there is not such card registrar available'),
      'Expired Card Pick Up'                 => t('card has been expired'),
      'Incorrect PIN'                        => t('pin number is incorrect'),
      'No Sufficient Funds'                  => t('not enough deposit in your account'),
      'Issuer Down Slm'                      => t('card registrar bank system is not ready'),
      'TME Error'                            => t('error in bank network'),
      'Exceeds Withdrawal Amount Limit'      => t('amount of money is beyond the withdrawal limit'),
      'Transaction Cannot Be Completed'      => t('can not register the document'),
      'Allowable PIN Tries Exceeded Pick Up' => t('card password has been entered incorrectly for 3 times, your card is disabled now'),
      'Response Received Too Late'           => t('bank network transaction time-out'),
      'Suspected Fraud Pick Up'              => t('CCV2 or ExpDate fields is incorrect')
    );
  }

  /**
   * Generate a (pseudo-random) unique ResNum
   */
  protected function createResNum() {
    do {
      $m = md5(microtime());
      $resNum = substr($m, 0, 20);
      $sql = "
      SELECT count(res_num) FROM {uc_payment_mellat}
      WHERE res_num =':num'
      ";
      $searchC = db_query($sql, array(':num' => $resNum))->fetchField();
      if ($searchC < 1 ) {
        break;
      }
    } while(TRUE);

    $this->resNum = $resNum;
  }

  /**
   * Search and find a ResNum in database.
   */
  protected function searchResNum($id) {
    $query = db_select('uc_payment_mellat', 'ps');
    $result = $query->fields('ps')
      ->condition('id', $id)
      ->orderBy("id", "DESC")
      ->range(0,1)
      ->execute()
      ->fetchAssoc();

    return $result;
  }

  /**
   * Search and find a RefNum in database.
   */
  protected function searchRefNum($refNum) {
    $query = db_select('uc_payment_mellat', 'ps');
    $result = $query->fields('ps')
      ->condition('ref_num', $refNum)
      ->execute()
      ->fetchAssoc();

    return $result;
  }

  /**
   * Update and save bank information in the database.
   *
   * Updates the ref_num and payment in DB based on $this->resNum.
   *
   * @param $payment
   *   The total paid amount (via bank) (if payment is successfull, this should be equal to
   *   the total amount of cart.)
   */
  protected function saveBankInfo($payment) {
    $this->payment = $payment;
    //TODO: See Original code (error handling)
    db_update('uc_payment_mellat')
      ->fields(array(
        'ref_num' => $this->refNum,
        'payment' => $payment,
      ))
      ->condition('id', $this->idMellat)
      ->execute();

    return TRUE;
  }

  /**
   * Insert and save the Store info into the database.
   *
   * Inserts new record in uc_payment_mellat table without the ref_num and payment fields.
   * those two fields will be updated later after payment by the saveBankInfo() method.
   */
  public function saveStoreInfo($totalAmont, $id) {
    if ($totalAmont == '') {
      $this->setMsg("Error state : ","Error: TotalAmont");
      return FALSE;
    }

    $time = time();
    $this->totalAmont = $totalAmont;

    //check if record already exists (if user refreshes the page)
    /*$exists = db_select('uc_payment_mellat', 'ps')
      ->fields('ps')
      ->condition('id', $id)
      ->execute()
      ->fetchAssoc();*/
      $last_id = db_query('SELECT MAX(id) FROM {uc_payment_mellat}')->fetchField();
      $last_id = $last_id+1;
    if ($last_id>0) {
      //does not exist, create new record.
      //$this->createResNum();
      $this->resNum = $id;
          watchdog('uc_cart', '243 An empty order resNum= @resNum', array('@resNum' => $id), WATCHDOG_ERROR);
      $this->idMellat = $last_id;
      db_insert('uc_payment_mellat')
        ->fields(array(
          'id' => $last_id,
          'res_num' => $this->resNum,
          'total_amont' => $this->totalAmont,
          'date_start' => $time,
        ))
        ->execute();
    }
    /*else {
      //record already exists. load it to the object
      $this->resNum = $exists['res_num'];
    }*/

    return TRUE;
  }

  /**
   *
   */
  public function receiverParams($id = '', $refNum = '', $state = '',$sale_Refrencr_id ='' ) {
    if ($state != '0') {
      if (isset($this->errorVerify[$state])) {
        $this->setMsg('error state', $this->errorVerify[$state]);
      }
      else {
        $this->setMsg("Error state : ","error state".$state);
      }
       watchdog('uc_cart', '268 An empty order resNum= @resNum it to refNum = @refNum! state @state order ID: @cart_order', array('@resNum'=>$id,'@refNum'=>$sale_Refrencr_id,'@state'=>$state,'@cart_order' => $sale_Refrencr_id), WATCHDOG_ERROR);
      return FALSE;
    }
    $searchResNum = $this->searchResNum($id);
   /* if (is_array($searchResNum)) {
      if ($searchResNum['payment'] > 0) {
        $this->setMsg("Error state : ",t('Please go to pursuit area'));
          watchdog('uc_cart', '275 An empty order resNum= @resNum it to refNum = @refNum! state @state order ID: @cart_order', array('@resNum'=>$resNum,'@refNum'=>$sale_Refrencr_id,'@state'=>$state,'@cart_order' => $sale_Refrencr_id), WATCHDOG_ERROR);
        return FALSE;
      }
    }
    else {
      $this->setMsg("Error state : ",t('This Transaction does not difined at serverside'));
       watchdog('uc_cart', '281 An empty order resNum= @resNum it to refNum = @refNum! state @state order ID: @cart_order', array('@resNum'=>$resNum,'@refNum'=>$sale_Refrencr_id,'@state'=>$state,'@cart_order' => $sale_Refrencr_id), WATCHDOG_ERROR);
      return FALSE;
    }*/
    watchdog('uc_cart', 'An empty order resNum= @resNum it to refNum = @refNum! state @state order ID: @cart_order', array('@resNum'=>$id,'@refNum'=>$sale_Refrencr_id,'@state'=>$state,'@cart_order' => $sale_Refrencr_id), WATCHDOG_ERROR);
    $resNum = $searchResNum['res_num'];
    $this->resNum = $resNum;
    $this->idMellat = $id;
    $this->refNum = $sale_Refrencr_id;
        db_update('uc_payment_mellat')
          ->fields(array(
        'ref_num' => $this->refNum,
      ))
      ->condition('res_num', $this->resNum)
      ->execute();
    $this->refNum = $sale_Refrencr_id;
    $this->resNum = $resNum;
    $this->totalAmont = $searchResNum['total_amont'];
    return $this->lastCheck();
  }

  /**
   *
   */
  protected function lastCheck() {
    if (empty($this->resNum) || strlen($this->refNum) < 0) {
      $this->setMsg("Error state : ","Error: resNum or refNum is empty");
      return FALSE;
    }
    //web method verify transaction
    $verify = $this->verifyTrans();
                    watchdog('uc_cart', 'An empty order verify= @resNum it to ref = @refNum! state order ID:', array('@resNum'=>$verify,'@refNum'=>$this->refNum), WATCHDOG_ERROR);

    $this->setMsg('verify1', $this->errorVerify[$verify]);
    //dpm("funstion lastCheck() ==> \$this->verifyTrans() returned: \$verify = "
    //    . print_r($verify, TRUE));

    if ($verify == "0") {
        $this->saveBankInfo($this->totalAmont);
        $this->setMsg("successfully state : ",t('Amount has been successfully paid . please note pursuit code.'));
        $this->setMsg("successfully state : ",t('pursuit code : @code', array('@code' => $this->resNum)));
        return TRUE;
      //Error transaction
    }
    else {
      $this->setMsg("Error state : ",t('Dear user , an error occured for payment'));
      $this->setMsg('verify1', $this->errorVerify[$verify]);
      $this->saveBankInfo(0);
      return FALSE;
    }
  }

  /**
   * Verify transaction
   */
  protected function verifyTrans() {
    if (empty($this->refNum) || empty($this->merchantID)) {
      return FALSE;
    }

    //$soapClient = new soapclient($this->webMethodURL);
    //$soapProxy = $soapClient;
    $result = FALSE;
    $parameters = array(
      'terminalId' => $this->merchantID,
      'userName' => $this->userName,
      'userPassword' => $this->password,
      'orderId' => $this->idMellat,
      'saleOrderId' => $this->idMellat,
      'saleReferenceId' => $this->refNum,
  );
    watchdog('uc_cart', 'An empty order merchantID= @resNum it to refNum = @refNum! state order ID:', array('@resNum'=>$this->merchantID,'@refNum'=>$this->refNum), WATCHDOG_ERROR);

  $client = new nusoap_client($this->webMethodURL);
    if ($client->getError()) {
      return FALSE;
    }

    // Verify payment with bpVerifyRequest method.
    $verify_result = $client->call('bpVerifyRequest', $parameters, 'http://interfaces.core.sw.bps.com/');
        watchdog('uc_cart', 'An empty order verify_result= @resNum it to refNum = @refNum! state order ID:', array('@resNum'=>$verify_result,'@refNum'=>$this->refNum), WATCHDOG_ERROR);

    $this->setMsg('verify1', $verify_result);
    if ($client->fault || $client->getError()) {
      $verify_result = $client->call('bpInquiryRequest', $parameters, 'http://interfaces.core.sw.bps.com/');
      if ($client->fault || $client->getError()) {
        $client->call('bpReversalRequest', $parameters, 'http://interfaces.core.sw.bps.com/');
        return FALSE;
      }
    }

    return $verify_result;
  }

  /**
   * Reverse transaction
   */
  protected function reverseTrans($revNumber) {
    if ($revNumber <= 0 || empty( $this->refNum) || empty($this->merchantID) || empty($this->password)) {
      return FALSE;
    }
    $soapClient = new soapclient($this->webMethodURL);
    $soapProxy = $soapClient;//->getProxy();
    $result = FALSE;

    for ($a = 1; $a < 6; ++$a ) {
      $result = $soapProxy->reverseTransaction($this->refNum, $this->merchantID, $this->password, $revNumber);
      if ($result != FALSE) {
        break;
      }
    }

    return $result;
  }

  /**
   *
   */
  public function sendParams() {
    require_once dirname(__FILE__) . '/lib/nusoap/nusoap.php';
    
    watchdog('uc_cart', '243 An empty order resNum= @resNum ii @ii', array('@resNum' => $this->resNum,'@ii' => $this->totalAmont), WATCHDOG_ERROR);
      if ($this->totalAmont <= 0 || empty($this->redirectURL) || empty($this->resNum) || empty($this->merchantID)) {
      $this->setMsg("Error state : ","Error: function sendParams()");
      return FALSE;
    }

//$zip_code = db_query("SELECT ref_num from {uc_payment_mellat} WHERE res_num = :res_num LIMIT 1", array(":res_num" => $this->resNum))->fetchField();
//if ($zip_code==null || strlen($zip_code)<5){
    $client = new nusoap_client($this->webMethodURL);
    $err = $client->getError();
    if ($err) {
      $this->setMsg('error state ',t('Error code: !error', array('!error' => check_plain($err))));
      return FALSE;
    }
  $parameters = array(
      'terminalId' => $this->merchantID,
      'userName' => $this->userName,
      'userPassword' => $this->password,
      'orderId' => $this->idMellat,
      'amount' => $this->totalAmont *10,
      'localDate' => date("Ymd"),
      'localTime' => date("His"),
      'additionalData' => '',
      'callBackUrl' => $this->redirectURL,
      'payerId' => 0,
  );
       watchdog('uc_cart', '436 An empty order resNum= @resNum ii @ii', array('@resNum' => $this->idMellat,'@ii' => $this->resNum), WATCHDOG_ERROR);

  $result = $client->call('bpPayRequest', $parameters, 'http://interfaces.core.sw.bps.com/');
  if ($client->fault || $client->getError()) {
    drupal_set_message(t('can not call service soap is invalid' ),'error');
    return FALSE;
  } else {
    $result = explode(',', $result);
    $result_code = $result[0];
    if ($result_code == "0") {
      $this->refNum = $result[1];
        db_update('uc_payment_mellat')
          ->fields(array(
        'ref_num' => $this->refNum,
      ))
      ->condition('res_num', $this->resNum)
      ->execute();
      return $this->refNum;
    } else {
      $this->setMsg("Error state : ","error state ".$this->errorVerify[$result_code]);
      return FALSE;
    }
  }
/*}else{
  $this->refNum = $zip_code;
  return $this->refNum;
}*/

  }

  /**
   * Set messages in $this->msg[]
   */
  protected function setMsg($type = '', $index = '') {
    if ($type == 'state' && isset($this->errorState[$index])) {
      $this->msg[] = $this->errorState[$index];
    }
    elseif ($type == 'verify' && isset($this->errorVerify[$index])) {
      $this->msg[] = $this->errorVerify[$index];
    }
    elseif ($type != 'verify' && $type != 'state') {
      $this->msg[] = "$index";
      drupal_set_message(t('Mellat Bank: !SaleReferenceId', array('!SaleReferenceId' => $index)), 'status');
    }
  }

  /**
   * Get messages stored in the object as html formatted
   */
  public function getMsg($display = TRUE) {
    if (count($this->msg) == 0 ) {
      return array();
    }

    if ($display) {
      $msg = "<ul>\n";
      foreach ($this->msg as $v) {
        $msg .= "<li> $v </li>\n";
      }
      $msg .= "</ul>\n";

      return $msg;
    }
    else {
      return $this->msg;
    }
  }
}
